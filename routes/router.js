var router         = require("express").Router();

var Nominas        = require("./api/nominas.js");

var requestPrinter = require("../middlewares/request_printer.js");

var settings       = require("../settings.json");

// middlewares
router.use(function(request, response, next){
	response.header("Access-Control-Allow-Origin", "*");
	response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
	response.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
	next();
});

if(settings.debug){
	router.use("/", requestPrinter);
}

router.route("/api/")
	.options(function(request, response){
		response.status(200).send({
			server_name : "nominas-server",
			version     : "0.2.1",
			message     : "I'm alive!!! :D"
		});
	});

router.use("/api/nominas", Nominas);

module.exports = router;