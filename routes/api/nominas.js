var router 		   = require("express").Router();

var nominaController = require("../../controllers/nomina_controller.js");

/**
 * Route: /api/goods/page/:page
 * GET - Sends a page of good registers to the client.
 */
router.route("/payments/semestre")
	.get(function(request, response){
		nominaController.paymentsForCalendar(request.query)
			.then(function(page){
				response.send(page);
			})
			.catch(function(err){
				console.log(err);
				response.status(err.status ? err.status : 500).send(err);
			});
	});

/**
 * Route: /api/goods/page/:page
 * GET - Sends a page of good registers to the client.
 */
router.route("/payments/top-ten")
	.get(function(request, response){
		nominaController.paymentsTopTen(request.query)
			.then(function(page){
				response.send(page);
			})
			.catch(function(err){
				console.log(err);
				response.status(err.status ? err.status : 500).send(err);
			});
	});

router.route("/teachers/page/:page")
	.get(function(request, response){
		nominaController.getPage(request.query, request.params)
			.then(function(page){
				response.send(page);
			})
			.catch(function(err){
				response.status(err.status ? err.status : 500).send(err);
			});
	});

router.route("/teachers/:id")
	.get(function(request, response){
		nominaController.getTeacher(request.params)
			.then(function(collection){
				response.send(collection);
			})
			.catch(function(err){
				console.log(err);
				response.status(err.status ? err.status : 500).send(err);
			});
	});

module.exports = router;