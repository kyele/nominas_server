var bookshelf 		= require("../database/db.js").bookshelf;
var knex 			= require("../database/db.js").knex;

var Paysheet 		= require("../database/models/paysheet.js");
var Teacher 		= require("../database/models/teachers.js");

function paymentsForCalendar(query){
	return new Promise(function(resolve, reject){
		var calendar = query.calendar;
		var career   = query.career;
		Paysheet.query(function(builder){
			builder.
				columns(
					"payment",
					"name_job"
				)
				.count("calendar AS registros")
				.sum("payment AS total_payment")
				.where("calendar", "=", calendar)
				.where("career", "LIKE", "%" + career + "%");
			}
		)
		.fetch()
		.then(function(data){
			//console.log(data.attributes);
			resolve(data.attributes);
		})
		.catch(reject);
	});
}

function paymentsTopTen(query){
	return new Promise(function(resolve, reject){
		var calendar = query.calendar;
		var career   = query.career;
		var options = {
			pageSize : 10,
			page     : 1
		};
		Paysheet.query(function(builder){
			builder.
				columns(
					"name_job",
					"name",
					"last_name",
					"calendar"
				)
				.sum("payment AS total_salary")
				.where("calendar", "=", calendar)
				.where("career", "LIKE", "%" + career + "%")
				.groupBy("id_teacher")
				.orderByRaw("`total_salary` DESC");
			}
		)
		.fetchPage(options)
		.then(function(salaryCollection){
			var collection = [];

			salaryCollection.models.forEach(function(salaryModel){
				var salaryAttributes = salaryModel.attributes;
				collection.push(salaryAttributes);
			});
			resolve({
				collection : collection,
				pagination : salaryCollection.pagination
			});
			resolve(data.attributes);
		})
		.catch(reject);
	});
}

function getPage(query, params){
	return new Promise(function(resolve, reject){
		var filter = query.filter;
		var column = query.column ? query.column : "name";
		var order  = query.order == 0 ? "desc" : "asc";

		var pageSize = query.page_size > 0 && query.page_size <= 100 ? query.page_size : 25;
		var page = params.page > 0 ? params.page : 1;

		Teacher.query(function(builder){
			builder.select("*");

			if(filter)
			{
				builder.orWhere("name", "LIKE", "%" + filter + "%")
					   .orWhere("last_name", "LIKE", "%" + filter + "%")
					   .orWhere("career", "LIKE", "%" + filter + "%");
			}
		})
		.orderBy(column, order)
		.fetchPage({
			pageSize: pageSize,
			page: page
		})
		.then(function(collection){
			resolve({
				collection: collection.models, 
				pagination: collection.pagination
			});
		})
		.catch(reject);
	});
}

function getTeacher(params){
	return new Promise(function (resolve , reject){
		var data;
		var options = {
			withRelated : ["payments"]
		};
		new Teacher({id : params.id})
			.fetch(options)
			.then(function(teacherModel){
				if(!teacherModel){
					reject({
						status  : 404,
						label   : "NOT_FOUND",
						message : "Requested 'branch_office' with ID [" + id + "] was not found"
					});
				}
				else{
					var paymentsCollection = teacherModel.related("payments").models.map(teacherModel => teacherModel.attributes);

					var payments = {};
					var count = 0;
					//payments["PROFESOR ASIGNATURA"] = 500;
					//console.log(paymentsCollection);
					
					for(payment in paymentsCollection) {
						var job = paymentsCollection[payment].name_job;

						if(payments[job]) {
							payments[job].total_salary = payments[job].total_salary + paymentsCollection[payment].payment;
							payments[job].salarys = paymentsCollection[payment].payment > 0 ? payments[job].salarys + 1 : payments[job].salarys;
						} else {
							var before_job = count > 0 ? paymentsCollection[payment-1].name_job : null;
							if(payments[before_job]){
								payments[before_job].mes_end  = paymentsCollection[payment-1].month;
								payments[before_job].anio_end = parseInt(paymentsCollection[payment-1].calendar.substr(0,4));
							}
							payments[job] = {};
							payments[job].total_salary = paymentsCollection[payment].payment;
							payments[job].salarys      = 1;
							payments[job].mes_started  = paymentsCollection[payment].month;
							payments[job].anio_started = parseInt(paymentsCollection[payment].calendar.substr(0,4));
						}
						count ++;
					}
					var before_job = paymentsCollection[count-1].name_job;
					payments[before_job].mes_end  = paymentsCollection[count-1].month;
					payments[before_job].anio_end = parseInt(paymentsCollection[count-1].calendar.substr(0,4));
					if(isNaN(payments[before_job].anio_end)){
						payments[before_job].anio_end = "2017";
					}
					console.log(payments);
					data = teacherModel.attributes;
					data.payments = payments;
					
					resolve(data);
				}
			})
			.catch(function(err){
				console.log(err);
				reject({
					status  : 500,
					label   : "DATABASE_ERROR",
					message : "There was an error with the database",
					error   : err
				});
			});
	});
}

module.exports = {
	paymentsForCalendar,
	paymentsTopTen,
	getPage,
	getTeacher
};