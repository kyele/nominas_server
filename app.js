var express        = require("express");
var router         = require("./routes/router.js");
var bodyParser     = require("body-parser");

var app            = express();

var settings       = require("./settings.json");
//public paths
app.use("/public", express.static("public"));

// Middlewares install
app.use(bodyParser.json({limit: "5mb"}));
app.use(bodyParser.urlencoded({limit: "5mb", extended: true}));

// Router install
app.use("/", router);

//start server
app.listen(settings.api_rest.port);