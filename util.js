/**
 * Maps an enum turning it into another object but using the values as keys and viceversa.
 * 
 * @param  {Object|Enum} myEnum - Represent the enum itself, it must be in format:
 *                                {
 *                                    ENUM_VAL_1 : 1,
 *                                    ENUM_VAL_2 : 2,
 *                                    ENUM_VAL_N : N
 *                                }
 * @return {Object} Returns an object with the following format:
 *                  {
 *                      '1' : 'ENUM_VAL_1',
 *                      '2' : 'ENUM_VAL_2',
 *                      'N' : 'ENUM_VAL_N'
 *                  }
 */
var mapEnum = myEnum => Object.keys(myEnum).reduce((map, key) => {map[myEnum[key]] = key; return map;}, {});

module.exports = {
	mapEnum
};