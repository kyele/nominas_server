var settings = require("../settings.json");

var dbConfig = {
	client: "mysql",
	connection: {
		host:     settings.database.host,
		port:     settings.database.port,
		user:     settings.database.user,
		password: settings.database.password,
		database: settings.database.database,
		charset:  settings.database.charset
	},
	pool: {
		min: 1, 
		max: 100
	},
	acquireConnectionTimeout: 10000,
	debug:                    settings.debug
};

var knex = require("knex")(dbConfig);
var bookshelf = require('bookshelf')(knex);

bookshelf.plugin("registry");
bookshelf.plugin('pagination');

module.exports = {
	dbConfig: dbConfig,
	knex: knex,
	bookshelf: bookshelf
};