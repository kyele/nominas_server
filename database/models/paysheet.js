var bookshelf 	= require("../db.js").bookshelf;

var Paysheet = bookshelf.Model.extend({
	tableName: "paysheet"
});

module.exports = bookshelf.model("Paysheet", Paysheet);