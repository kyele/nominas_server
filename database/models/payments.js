var bookshelf 	= require("../db.js").bookshelf;

var Teacher 	= require("./teachers.js");

var Payments = bookshelf.Model.extend({
	tableName: "payments",
	teacher: function(){
		return this.belongsTo('Teacher' , 'teacher_id');
	}
});

module.exports = bookshelf.model("Payments", Payments);