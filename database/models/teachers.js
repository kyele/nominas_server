var bookshelf = require("../db.js").bookshelf;
var knex 	  = require("../db.js").knex;

var Payments   = require("./payments.js");

var Teachers = bookshelf.Model.extend({
	tableName: "teachers",
	payments : function () {
	 	return this.hasMany('Payments' , 'id_teacher');
   	}
});

module.exports = bookshelf.model("Teachers", Teachers);